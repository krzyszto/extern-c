#include <iostream>
#include "network.h"
#include <map>
#include <set>

#ifndef NDEBUG
    const bool debug = true;
#else
    const bool debug = false;

#endif

using namespace std;
typedef map<string, set<string> > web;

int& curr_id()
{
	static int curr = 0;
	return curr;
}
set<int>& growings()
{
	static set<int> growing_nets;
	return growing_nets;	
}

map<int, web>& w()
{
	static map<int, web> webs;
	return webs;	
}

unsigned long network_new(int growing)
{
	web web_n;
	w().insert(pair<int,web>(curr_id()++, web_n));
	if(debug){
		cerr << "tworze siec o id " << curr_id() - 1 << endl;
	}
	if (growing) 
		growings().insert(curr_id() - 1);
	return curr_id() - 1;
}

void network_delete(unsigned long id)
{	
	map<int, web>::iterator it = w().find(id);
	if (it != w().end()){
		w().erase(it);
		if(debug)
			cerr << "usuwa siec o id " << id << endl;
	}
	else {
		if(debug)
			cerr << "nie ma sieci o id " << id << endl;
	}
}

size_t network_nodes_number(unsigned long id)
{
	map<int, web>::iterator it = w().find(id);
	if (it != w().end()) {
		if(debug)
			cerr << "siec o id " << id << " ma "
			<< it->second.size() << " wezlow" << endl;
		return it->second.size();
	}
	else {
		return 0;
	}
}

size_t network_links_number(unsigned long id)
{
	int sum = 0;
	map<int, web>::iterator it = w().find(id);
	if (it != w().end()) {
		for (web::iterator i = it->second.begin();
		i != it->second.end(); i++){
			sum += i->second.size();
		}
		if(debug)
			cerr << "siec o id " << id << " ma " << sum
			<< " krawedzi" << endl;
		return sum;
	}
	else {
		return 0;
	}
}

void network_add_node(unsigned long id, const char* label)
{
	map<int, web>::iterator it = w().find(id);
	if (it != w().end()) {
		if(label != NULL){
			web::iterator it_node = it->second.find(label);
			if(it_node == it->second.end()){
				set<string> edge_set;
				it->second.insert(pair<string,set<string> >
				(label, edge_set));
				if(debug)
					cerr << "dodalem wezel " << label <<
					" do sieci o id " << id << endl;
			}
		} else if(debug){
			cerr << "do sieci o id " << id <<
			" probowano dodac wezel o etykiecie null" << endl;
		}
	}
	
	
}

void network_add_link(unsigned long id, const char* slabel, const char* tlabel)
{
	map<int, web>::iterator it = w().find(id);
	if (it != w().end()) {
		if((slabel != NULL) && (tlabel != NULL)) {
			if(it->second.find(slabel) == it->second.end())
				network_add_node(id, slabel);
			if(it->second.find(tlabel) == it->second.end())
				network_add_node(id, tlabel);
			web::iterator it_node = it->second.find(slabel);
			it_node->second.insert(tlabel);
			if(debug)
					cerr << "dodalem krawedz z " <<
					slabel << "-> " << tlabel <<
					" do sieci o id " << id << endl;
		} else if(debug){
			cerr << "do sieci o id " << id <<
			" probowano dodac krawedz o koncu w wierzchołku "<<
			" o etykiecie NULL" << endl;
		}
	} else if (debug) {
		cerr << "probowano dodac krawedz do nieistniejacej sieci o id "
		<< id << endl;
	}
}

void network_remove_node(unsigned long id, const char* label)
{
	map<int, web>::iterator it = w().find(id);
	if (it != w().end()) {
		if(!growings().count(it->first)){
			it->second.erase(label);
			if(debug)
				cerr << "usuwam wezel " << label <<
				" z sieci o id " << id << endl;
			for(web::iterator i = it->second.begin();
			i != it->second.end(); ++i){
				i->second.erase(label);
			}
		} else if(debug){
			cerr << "nie mozna usunac wezla z sieci o id " <<
			id << " poniewaz ta siec jest rosnaca" << endl;
		}
	} else if(debug){
		cerr << "probowano usunac wezel z sieci o id " << id <<
		" lecz taka siec nie istnieje" << endl;
	}
}

void network_remove_link(unsigned long id, const char* slabel, const char* tlabel)
{
	map<int, web>::iterator it = w().find(id);
	if (it != w().end()) {
		if(!growings().count(it->first)) {
			web::iterator it_node = it->second.find(slabel);
			it_node->second.erase(tlabel);
			if(debug)
					cerr << "usuwam krawedz z " << slabel <<
					"-> " << tlabel << " z sieci o id " <<
					id << endl;
		} else if (debug){
			cerr << "probowano usunac krawedz z sieci o id " <<
			id << " lecz podana siec jest rosnaca" << endl;
		}
	} else if (debug) {
		cerr << "siec o identyfikatorze " << id <<
		" nie istnieje, wiec nie mozna usunac krawedzi" << endl;
	}
}

void network_clear(unsigned long id)
{
	map<int, web>::iterator it = w().find(id);
	if (it != w().end()) {
		if(!growings().count(it->first)) {
			it->second.clear();
			if(debug)
					cerr << "czyszcze siec o id " << id <<
					endl;
		}
	} else if (debug) {
		cerr << "siec o id " << id
		<< " nie istnieje, wiec nie mozna jej wyczyscic" << endl;
	}
}

size_t network_out_degree(unsigned long id, const char* label)
{	size_t val = 0;
	map<int, web>::iterator it = w().find(id);
	if (it != w().end()) {
		web::iterator it_node = it->second.find(label);
		if(it_node != it->second.end()){
			val = it_node->second.size();
		}
	}
	if(debug)
		cerr << "stopien wychodzacy wierzcholka " << label <<
		" w sieci o id " << id << " wynosi " << val << endl;
	return val;
}

size_t network_in_degree(unsigned long id, const char* label)
{
	size_t val = 0;
	map<int, web>::iterator it = w().find(id);
	if (it != w().end()) {
		for(web::iterator i = it->second.begin();
		i != it->second.end(); ++i){
			if(i->second.count(label)) val++;
		}
	}
	cerr << "stopien wchodzacy wierzcholka " << label << " w sieci o id " <<
	id << " wynosi " << val << endl;
	return val;			
}
