#ifndef GROWINGNET_H
#define GROWINGNET_H

#ifdef __cplusplus
extern "C" {
#endif
/*
 * Identyfikator Globalnej Sieci Rosnacej.
 */
extern const unsigned long growingnet;

#ifdef __cplusplus
}
#endif

#endif
