#ifndef NETWORK_H
#define NETWORK_H

#ifdef __cplusplus
extern "C" {
#endif
/*
 * Tworzy nowa, siec z pustym zbiorem wezlow i podaje jej identyfikator.
 * growing - jesli jest rozny od 0 to siec jest rosnaca,
 * w przeciwnym przypadku jest nierosnaca.
 */
unsigned long network_new(int growing);

/*
 * Ponizsze instrukcje nic nie wykonuja, jezeli nie da sie wykonac
 * instrukcji z powodu blednych parametrow, tzn takich, ktore nie istnieja
 * lub opisuja nieistniejacy byt, lub kiedy dana operacja nie jest dozwolona
 * w danym przypadku (na przyklad usuniecie wezla z sieci rosnacej).
 */

/*
 * Usuwa siec o identyfikatorze podanym jako parametr.
 */
void network_delete(unsigned long id);

/*
 * Podaje liczbe wezlow sieci o identyfikatorze podanym w parametrze.
 * Siec nieistniejaca ma liczbe wezlow rowna 0.
 */
size_t network_nodes_number(unsigned long id);

/*
 * Podaje liczbe krawedzi sieci o identyfikatorze podanym w parametrze.
 * Siec nieistniejaca ma liczbe krawedzi rowna 0.
 */
size_t network_links_number(unsigned long id);

/*
 * Jesli istnieje siec o identyfikatorze id, probuje dodac wezel
 * o etykiecie podanej przez parametr label.
 */
void network_add_node(unsigned long id, const char* label);

/*
 * Dodaje do sieci o podanym ID wezel pomiedzy koncami slabel i tlabel.
 * Identyfikatory koncow nie moga byc nullami.
 * Jezeli nie istnieje wezel o podanym indentyfikatorze, jest on tworzony.
 */
void network_add_link(unsigned long id, const char* slabel, const char* tlabel);

/*
 * Usuwa wezel z sieci o podanym ID wezel o danej etykiecie
 * razem ze wszystkimi wezlami wchodzacymi i wychodzacymi z danego wezla.
 */
void network_remove_node(unsigned long id, const char* label);

/*
 * Usuwa krawedz pomiedzy podanymi wezlami w sieci o danym ID.
 */
void network_remove_link(unsigned long id, const char* slabel,
                         const char* tlabel);

/*
 * Usuwa wszelkie wezly i krawedzie z sieci i podanym ID.
 */
void network_clear(unsigned long id);

/*
 * Podaje liczbe krawedzi wychodzacych z wezla okreslonego przez ID sieci
 * i etykiete wezla. Gdy wezel nie istnieje wynikiem jest 0.
 */
size_t network_out_degree(unsigned long id, const char* label);

/*
 * Podaje liczbe krawedzi wchodzacych do wezla okreslonego przez ID sieci
 * i etykiete wezla. Gdy wezel nie istnieje wynikiem jest 0.
 */
size_t network_in_degree(unsigned long id, const char* label);

#ifdef __cplusplus
}
#endif

#endif
