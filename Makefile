CC=gcc
CXX=g++
CXXFLAGS= -Wall 
DEBUGFLAGS   = -O0 -D _DEBUG
RELEASEFLAGS = -O2 -D NDEBUG 

ifeq ($(debuglevel), 1)
    FLAGS = $(DEBUGFLAGS)
else
    FLAGS= $(RELEASEFLAGS)
endif

all: network.o growingnet.o

growingnet.o: growingnet.cc 
	$(CXX) $(CXXFLAGS) $(FLAGS) -c growingnet.cc -o growingnet.o
	
network.o: network.cc 
	$(CXX) $(CXXFLAGS) $(FLAGS) -c network.cc  -o network.o
	
clean:
	rm -f *.o

.PHONY: clean all
	
